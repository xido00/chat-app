import { all, put, fork, takeEvery } from 'redux-saga/effects';
import { syncDatabaseSaga, create, read } from './database';

import {
  types,
  roomsSync
} from '../actions/rooms';

import { mapRooms } from '../utils/rooms';

function* roomAdd(data) {
  return yield create('/rooms/', data.room);
}

function* roomAddMessage(data) {
  return yield create(`/rooms/${data.room}/messages`, data.message);
}

function* readRooms() {
  const data = yield read('/rooms/');
  const action = mapRoomsAndMessages(data);
  yield put(action);
}

function mapRoomsAndMessages(data) {
  const mapped = mapRooms(data);
  return roomsSync(mapped);
}

export default function* roomsRootSaga() {
  yield fork(syncDatabaseSaga, 'rooms', mapRoomsAndMessages)
  yield all([
    takeEvery(types.ROOM.ADD, roomAdd),
    takeEvery(types.ROOMS.REQUEST, readRooms),
    takeEvery(types.MESSAGE.ADD, roomAddMessage)
  ])
}
