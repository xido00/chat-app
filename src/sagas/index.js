import { fork } from 'redux-saga/effects';

import login from './login';
import rooms from './rooms';

export default function* root () {
  yield fork(login)
  yield fork(rooms)
};
