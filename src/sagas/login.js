import firebase from '../utils/firebase';
import { eventChannel } from 'redux-saga';
import { call, fork, put, all, take, takeEvery } from 'redux-saga/effects';

import {
  types,
  login,
  logout,
  loginSuccess,
  loginFailure,
  logoutSuccess,
  logoutFailure,
  userSync
} from '../actions/login';

let _authChannel;

function getCurrentUser() {
  return firebase.auth().currentUser;
}

function authChannel() {
  if (_authChannel) return _authChannel;

  const auth = firebase.auth();
  const channel = eventChannel(emit => {
    const unsubscribe = auth.onAuthStateChanged(
      user => emit({ user }),
      err => emit({ err })
    );

    return unsubscribe
  });

  _authChannel = channel;
  return channel;
}

function* signInAnonymously () {
  const auth = firebase.auth();
  return yield call([auth, auth.signInAnonymously])
}

function* loginSaga() {
  try {
    const data = yield call(signInAnonymously);
    yield put(loginSuccess(data.toJSON()));
    return data;
  } catch (err) {
    yield put(loginFailure())
  }
}

function* logoutSaga() {
  try {
    const auth = firebase.auth();
    yield put(logout());
    yield call([auth, auth.signOut]);
    yield put(logoutSuccess());
  } catch (err) {
    yield put(logoutFailure());
  }
}

function* updateUserSaga(data) {
  try {
    let user = getCurrentUser();
    if (!user) {
      yield put(login());
      yield call(loginSaga);
      user = getCurrentUser();
    }
    yield call([user, user.updateProfile], data.user);
    yield put(userSync(user.toJSON()));
  } catch (err) {
      yield put(userSync({}));
  }
}

function* syncUserSaga () {
  const _channel = yield call(authChannel);

  while(true) {
    const { user } = yield take(_channel);
    if (user) {
      yield put(userSync(user.toJSON()));
    } else {
      yield put({ type: types.LOGIN.REQUEST });
    }
  }
}

export default function * loginRootSaga() {
  yield fork(syncUserSaga)
  yield all([
    takeEvery(types.LOGIN.REQUEST, loginSaga),
    takeEvery(types.LOGOUT.REQUEST, logoutSaga),
    takeEvery(types.USER.UPDATE, updateUserSaga)
  ])
}
