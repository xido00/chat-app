import firebase from '../utils/firebase';
import { eventChannel } from 'redux-saga';
import { call, put, take } from 'redux-saga/effects';

const ref = path => firebase.database().ref(path);

export function* create(path, data) {
  const _ref = ref().child(path);
  const result = yield call([_ref, _ref.push], data);

  return result.key
}

export function* read(path, event = 'value') {
  const _ref = ref(path);
  return yield call([_ref, _ref.once], event);
}

function databaseChannel(path, event = 'value') {
  const channelRef = ref().child(path);

  const channel = eventChannel(emit => {
    const callback = channelRef.on(
      event,
      dataSnapshot => emit(dataSnapshot)
    );

    return () => ref().off(event, callback);
  });

  return channel;
}

export function* syncDatabaseSaga (path, callback) {
  const _channel = yield call(databaseChannel, path);

  while(true) {
    const response = yield take(_channel);
    if (response) {
      yield put(callback(response));
    }
  }
}
