import { connect } from 'react-redux';
import {
  getActiveRoomName,
  getMessagesOfActiveRoom
} from '../utils/rooms';
import { messageAdd } from '../actions/rooms';
import MessagesListComponent from '../components/MessagesList';


const mapDispatchToProps = dispatch => ({
  messageAdd: (room, message) => {
    dispatch(messageAdd(room, message))
  }
});

export const MessagesList = connect(state => {
  return {
    room: {
      id: state.rooms.active,
      name: getActiveRoomName(state.rooms)
    },
    messages: getMessagesOfActiveRoom(state.rooms),
    user: {
      userName: state.login.user.displayName || '',
      userId: state.login.user.uid || ''
    }
  }
}, mapDispatchToProps)(MessagesListComponent);
