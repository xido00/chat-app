import { connect } from 'react-redux';
import AddRoomComponent from '../components/AddRoom';

import { roomAdd } from '../actions/rooms';

const mapDispatchToProps = dispatch => ({
  roomAdd: (room) => {
    dispatch(roomAdd(room))
  }
});

export const AddRoom = connect(state => ({
  user: {
    owner: state.login.user.displayName || '',
    ownerId: state.login.user.uid || ''
  }
}), mapDispatchToProps)(AddRoomComponent);
