import { connect } from 'react-redux';
import RoomsListComponent from '../components/RoomsList';
import { roomOpen } from '../actions/rooms';

const mapDispatchToProps = dispatch => ({
  roomOpen: (roomId) => {
    dispatch(roomOpen(roomId))
  }
});

export const RoomsList = connect((state) => ({
  rooms: state.rooms.rooms
}), mapDispatchToProps)(RoomsListComponent);
