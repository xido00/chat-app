import { connect } from 'react-redux';
import SpinnerComponent from '../components/Spinner';

export const Spinner = connect(state => {
  return {
    loading: state.login.loading
  };
}, { })(SpinnerComponent);
