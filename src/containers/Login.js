import { connect } from 'react-redux';
import LoginComponent from '../components/Login';
import { userUpdate } from '../actions/login';
import { roomsRequest } from '../actions/rooms';

const mapDispatchToProps = dispatch => ({
  userUpdate: (displayName) => {
    dispatch(userUpdate({ displayName }))
    dispatch(roomsRequest())
  }
});

export const Login = connect(state => ({
  login: state.login
}), mapDispatchToProps)(LoginComponent);
