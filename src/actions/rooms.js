export const types = {
  ROOM: {
    ADD: 'ROOM.ADD',
    OPEN: 'ROOM.OPEN'
  },
  ROOMS: {
    SYNC: 'ROOMS.SYNC',
    REQUEST: 'ROOMS.REQUEST'
  },
  MESSAGE: {
    ADD: 'MESSAGE.ADD'
  }
};

export const roomOpen = id => ({
  type: types.ROOM.OPEN,
  id
});

export const roomAdd = room => ({
  type: types.ROOM.ADD,
  room
});

export const roomsSync = rooms => ({
  type: types.ROOMS.SYNC,
  rooms
});

export const roomsRequest = () => {
  console.log('rooms request');
  return {
    type: types.ROOMS.REQUEST
  }
};

export const messageAdd = (room, message) => ({
  type: types.MESSAGE.ADD,
  room,
  message
});
