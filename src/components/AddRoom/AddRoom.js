import React from 'react';
import PropTypes from 'prop-types';

import './AddRoom.css';

const AddRoom = (props) => {
  let input;

  return (
    <section className="add-room">
      <form className="form" onSubmit={(e) => {
        e.preventDefault();

        const { value } = input;
        if (value.length > 0) {
          props.roomAdd({
            name: value,
            creationDate: Date.now(),
            messages: [],
            ...props.user
          });
          input.value = '';
        }
      }}>
        <label htmlFor="room">Add room</label>
        <input
          type="text"
          id="room"
          className="input"
          placeholder="Type a name"
          autoComplete="off"
          ref={(node) => { input = node }}
        />
        <button type="submit" className="button" name="submit"><span>+</span></button>
      </form>
    </section>
  )
};

AddRoom.propTypes = {
  roomAdd: PropTypes.func.isRequired,
  user: PropTypes.shape({
    owner: PropTypes.string.isRequired,
    ownerId: PropTypes.string.isRequired
  }).isRequired
};

export default AddRoom;
