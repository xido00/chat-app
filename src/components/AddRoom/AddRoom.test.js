import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AddRoom from './AddRoom';

const setup = () => {
  const props = {
    roomAdd: jest.fn(),
    user: {
      owner: 'User',
      ownerId: 'user-1'
    }
  };

  Enzyme.configure({ adapter: new Adapter() })
  const wrapper = mount(<AddRoom {...props} />);

  return {
    props,
    wrapper
  }
};

describe('<AddRoom />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.add-room').length).toBe(1);
  });
});
