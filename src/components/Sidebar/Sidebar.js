import React from 'react';
import { hasUserADisplayName } from '../../utils/login';

import { RoomsList } from '../../containers/RoomsList';
import { AddRoom } from '../../containers/AddRoom';

import './Sidebar.css';

const Sidebar = (props) => {
  let classes = ['sidebar', 'is-hidden'];
  const { loading, user } = props.login;

  if (!loading && hasUserADisplayName(user)) {
    classes = ['sidebar'];
  }

  return (
    <aside className={ classes.join(' ') }>
      <h1>Simple Chat App</h1>
      <RoomsList />
      <AddRoom />
    </aside>
  )
}

export default Sidebar;
