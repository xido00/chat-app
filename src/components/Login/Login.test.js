import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Login from './Login';

const setup = () => {
  const props = {
    userUpdate: jest.fn(),
    login: {
      loggedIn: true,
      user: {
        displayName: ''
      }
    }
  };

  Enzyme.configure({ adapter: new Adapter() })
  const wrapper = mount(<Login {...props} />);

  return {
    props,
    wrapper
  }
};

describe('<Login />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.login').length).toBe(1);
  });
});
