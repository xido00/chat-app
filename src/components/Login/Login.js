import React from 'react';
import PropTypes from 'prop-types';

import { hasUserADisplayName } from '../../utils/login';

import './Login.css';

const Login = (props) => {
  const { user } = props.login;
  let name;

  if (hasUserADisplayName(user)) return false;

  return (
    <section className="login">
      <div className="login__container">
        <h1>Welcome in Chat App!</h1>
        <form onSubmit={(e) => {
          e.preventDefault();
          const value = name.value;
          if (value.length > 0) {
            props.userUpdate(value);
            name.value = '';
          }
        }}>
          <label htmlFor="name" className="login__label">What's your name?</label>
          <input type="text" name="name" className="input login__input" ref={ input => name = input } />
          <button type="submit" className="button login__submit">Join to chat</button>
        </form>
      </div>
    </section>
  );
}

Login.propTypes = {
  userUpdate: PropTypes.func.isRequired
};

export default Login;
