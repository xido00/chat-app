import React from 'react';
import PropTypes from 'prop-types';

import './Message.css';

const Message = ({ message, userName }) => (
  <li className="message">
    <div className="message__author">{ userName }</div>
    <p>{ message }</p>
  </li>
);

Message.propTypes = {
  message: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired
};

export default Message;
