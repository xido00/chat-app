import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Message from './Message';

const setup = () => {
  const props = {
    userName: 'John',
    message: 'Hello world!'
  };

  Enzyme.configure({ adapter: new Adapter() })
  const wrapper = mount(<Message {...props} />);

  return {
    props,
    wrapper
  }
};

describe('<Message />', () => {
  it('render the author', () => {
    const { props, wrapper } = setup();
    expect(wrapper.find('.message__author').text()).toBe(props.userName);
  });

  it('render the message', () => {
    const { props, wrapper } = setup();
    expect(wrapper.find('p').text()).toBe(props.message);
  });
});
