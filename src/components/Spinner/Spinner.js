import React from 'react';
import PropTypes from 'prop-types';

import './Spinner.css';

const Spiner = (props) => {
  let classes = ['spinner-wrapper'];

  if (!props.loading)
    classes.push('is-hidden');

  return (<div className={classes.join(' ')}>
    <div className="spinner">
      <div className="bounce1"></div>
      <div className="bounce2"></div>
      <div className="bounce3"></div>
    </div>
  </div>);
}

Spiner.propTypes = {
  loading: PropTypes.bool.isRequired
};

export default Spiner;
