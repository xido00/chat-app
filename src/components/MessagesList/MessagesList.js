import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Message from '../Message';
import AddMessage from '../AddMessage';

import './MessagesList.css';

const MessagesList = (props) => {
  const { room, messages, user, messageAdd } = props;

  if (room.name.length === 0) return null;

  return (
    <Fragment>
      <section className="messages">
        <h1 className="messages__heading">{ room.name }</h1>
        <ul className="messages__list">
          {
            messages.map(message => (
              <Message
                key={ message.id }
                { ...message }
              />
            ))
          }
        </ul>
      </section>
      <AddMessage { ...{ room, user, messageAdd } } />
    </Fragment>
  );
}

MessagesList.propTypes = {
  room: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  messages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired
  }).isRequired).isRequired,
  messageAdd: PropTypes.func.isRequired,
  user: PropTypes.shape({
    userName: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired
  }).isRequired
}

export default MessagesList;
