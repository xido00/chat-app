import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MessagesList from './MessagesList';

const setup = (_props = {}) => {
  const props = {
    room: {
      id: 'the-room',
      name: 'The Room'
    },
    messages: [
      {
        id: 'message-1',
        message: 'Message 1',
        userName: 'User 1'
      },
      {
        id: 'message-2',
        message: 'Message 2',
        userName: 'User 2'
      },
      {
        id: 'message-3',
        message: 'Message 3',
        userName: 'User 3'
      }
    ],
    user: {
      userName: 'User 1',
      userId: 'user-1'
    },
    messageAdd: jest.fn(),
    ..._props
  };

  Enzyme.configure({ adapter: new Adapter() })
  const wrapper = mount(<MessagesList {...props} />);

  return {
    props,
    wrapper
  }
};

describe('<MessagesList />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.messages').length).toBe(1);
  });

  it('should rednder empty component in room name is empty', () => {
    const { wrapper } = setup({ room: { id: '', name: '' }});
    expect(wrapper.find('.messages').length).toBe(0);
  });

  it('should render all messages', () => {
    const { props, wrapper } = setup();
    expect(wrapper.find('.messages__list').children().length).toBe(props.messages.length);
  });

  it('should render the title', () => {
    const { props, wrapper } = setup();
    expect(wrapper.find('.messages__heading').text()).toBe(props.room.name);
  });
});
