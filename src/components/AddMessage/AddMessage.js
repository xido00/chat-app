import React from 'react';
import PropTypes from 'prop-types';

import './AddMessage.css';

const AddMessage = (props) => {
  let input;

  if (props.room.id.length === 0) return null;

  return (
    <section className="add-message">
      <input
        type="text"
        name="message"
        className="input"
        autoComplete="off"
        placeholder={ `Message #${props.room.name}` }
        ref={(node) => { input = node }}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            const { value } = input;
            if (value.length > 0) {
              props.messageAdd(props.room.id, {
                message: value,
                creationDate: Date.now(),
                ...props.user
              });
              input.value = '';
            }
          }
        }}
      />
    </section>
  )
};

AddMessage.propTypes = {
  room: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  user: PropTypes.shape({
    userName: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired
  }).isRequired,
  messageAdd: PropTypes.func.isRequired
};

export default AddMessage;
