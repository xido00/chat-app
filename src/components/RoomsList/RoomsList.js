import React from 'react';
import PropTypes from 'prop-types';

import './RoomsList.css';

const RoomsList = (props) => (
  <section>
    <h1>Rooms</h1>
    <ul className="rooms">
      {
        props.rooms.map(room => (
          <li className="rooms__item" key={ room.id }>
            <small># </small>
            <a href={'/room/' + room.id} onClick={e => {
              e.preventDefault();
              props.roomOpen(room.id)
            }}>
              { room.name }
            </a>
          </li>
        ))
      }
    </ul>
  </section>
);

RoomsList.propTypes = {
  roomOpen: PropTypes.func.isRequired,
  rooms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired
};

export default RoomsList;
