import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RoomsList from './RoomsList';

const setup = () => {
  const props = {
    roomOpen: jest.fn(),
    rooms: [
      {
        id: '1',
        name: 'Room 1'
      },
      {
        id: '2',
        name: 'Room 2'
      },
      {
        id: '3',
        name: 'Room 3'
      }
    ]
  };

  Enzyme.configure({ adapter: new Adapter() })
  const wrapper = mount(<RoomsList {...props} />);

  return {
    props,
    wrapper
  }
};

describe('<RoomsList />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.rooms').length).toBe(1);
  });

  it('should render all child', () => {
    const { props, wrapper } = setup();
    const children = props.rooms.length;
    expect(wrapper.find('.rooms').children().length).toBe(children);
  });
});
