import React, { Component, Fragment } from 'react';

import { MessagesList } from './containers/MessagesList';
import { Login } from './containers/Login';
import { Sidebar } from './containers/Sidebar';
import { Spinner } from './containers/Spinner';

import './App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Sidebar />
        <main>
          <Spinner />
          <Login />
          <MessagesList />
        </main>
      </Fragment>
    );
  }
}

export default App;
