import {
  hasUserADisplayName
} from './login';

describe('Test utility functions for login', () => {
  it('return true if a user has a display name', () => {
    const user = {
      displayName: 'Username'
    };
    expect(hasUserADisplayName(user)).toBe(true);
  });

  it('return false if a user doesn\'t have a display name', () => {
    const user = {
      displayName: null
    };
    expect(hasUserADisplayName(user)).toBe(false);
    expect(hasUserADisplayName()).toBe(false);
  });
});
