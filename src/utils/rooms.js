export function mapRooms(_rooms) {
  const rooms = [];
  _rooms.forEach(room => {
    const mapped = { ...room.val(), id: room.key };
    Object.assign(mapped, {
      messages: mapObjectToArrayofObject({ ...mapped.messages })
    });
    rooms.push(mapped);
  });

  return rooms;
}

export function mapObjectToArrayofObject(object) {
  const keys = Object.keys(object);
  return keys.map(key => ({ id: key, ...object[key] }));
}

export function getActiveRoom(_rooms) {
  const { active, rooms } = _rooms;
  const index = findIndexOfRoom(rooms, active);

  return (index > -1) ? { ...rooms[index] } : {};
}

export function getMessagesOfActiveRoom(_rooms) {
  const room = getActiveRoom(_rooms);

  return ('messages' in room) ? room.messages : [];
}

export function getActiveRoomName(_rooms) {
  const room = getActiveRoom(_rooms);

  return ('name' in room) ? room.name : '';
}

export function findIndexOfRoom(rooms, id) {
  let f = -1;
  rooms.some((room, i) => {
    if (room.id === id) {
      f = i;
      return true;
    }

    return false;
  });

  return f;
}
