import * as firebase from 'firebase';
import firebaseConfig from './firebase.config';

firebaseConfig.debug = true;

export default firebase.initializeApp(firebaseConfig);
