import * as r from './rooms';

const rooms = [
  {
    id: 'room-1',
    name: 'Room 1',
    messages: []
  },
  {
    id: 'room-2',
    name: 'Room 2',
    messages: []
  },
  {
    id: 'room-3',
    name: 'Room 3',
    messages: [
      { message: 'message-1' },
      { message: 'message-2' },
      { message: 'message-3' },
    ]
  }
];

const indexActiveRoom = 2;
const roomsState = {
  active: rooms[indexActiveRoom].id,
  rooms
};

describe('Test utility functions for rooms', () => {
  it('should return index of a room with search id', () => {
    expect(r.findIndexOfRoom(rooms, rooms[1].id)).toEqual(1);
  });

  it('should returns active room object', () => {
    expect(r.getActiveRoom(roomsState)).toEqual(rooms[indexActiveRoom]);
  });

  it('should returns messages of active room', () => {
    expect(r.getMessagesOfActiveRoom(roomsState)).toEqual(rooms[indexActiveRoom].messages)
  });

  it('should returns the name of active room', () => {
    expect(r.getActiveRoomName(roomsState)).toEqual(rooms[indexActiveRoom].name)
  })
});
