export function hasUserADisplayName(user = {}) {
  if (!('displayName' in user)) return false;

  const { displayName } = user;

  return (typeof displayName === 'string' && displayName.length > 0)
}
