import { default as reducer, initialState }  from './login';
import * as actions from '../actions/login';

describe('login reducer tests', () => {
  it('reducer handles LOGIN.REQUEST action', () => {
    expect(
      reducer(initialState, actions.login())
    ).toEqual({
      ...initialState,
      loading: true
    });
  });

  it('reducer handles LOGIN.SUCCESS action', () => {
    const user = {
      uid: 'user-1',
      displayName: 'User 1'
    };

    expect(
      reducer(initialState, actions.loginSuccess(user))
    ).toEqual({
      loading: false,
      loggedIn: true,
      user
    });
  });

  it('reducer handles LOGIN.FAILURE and LOGOUT.SUCCESS and LOGOU.FAILURE action', () => {
    const newState = {
      ...initialState,
      loading: false
    };

    expect(
      reducer(initialState, actions.loginFailure())
    ).toEqual(newState);

    expect(
      reducer(initialState, actions.logoutSuccess())
    ).toEqual(newState);

    expect(
      reducer(initialState, actions.logoutFailure())
    ).toEqual(newState);
  });

  it('reducer handles USER.SYNC action', () => {
    const user = {
      uid: 'user-1',
      displayName: 'User 1'
    };

    expect(
      reducer(initialState, actions.userSync(user))
    ).toEqual({
      ...initialState,
      loading: false,
      loggedIn: true,
      user });
  });
});
