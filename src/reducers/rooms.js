import { types} from '../actions/rooms';

export const initialState = {
  active: '',
  rooms: []
}

function roomsReducer (state = initialState, action) {
  switch (action.type) {
    case types.ROOM.OPEN:
      return {
        ...state,
        active: action.id
      }
    case types.ROOMS.SYNC:
      const { rooms } = action;
      let { active } = state;

      if (rooms.length) {
        if (rooms.length < state.rooms.length) {
          if (!rooms.some(room => room.id === active)) {
            active = '';
          }
        }
        active = active.length ? active : rooms[0].id;
      } else {
        active = '';
      }

      return {
        ...state,
        active,
        rooms
      }
    default:
      return state;
  }
}

export default roomsReducer;
