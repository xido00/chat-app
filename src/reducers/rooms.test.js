import { default as reducer, initialState }  from './rooms';
import * as actions from '../actions/rooms';

const rooms = [
  {
    id: 'room-1',
    name: 'Room 1'
  },
  {
    id: 'room-2',
    name: 'Room 3'
  },
  {
    id: 'room-3',
    name: 'Room 3'
  }
];

describe('rooms reducer tests', () => {
  it('reducer handles ROOM.OPEN action', () => {
    const active = 'id-1';
    expect(
      reducer(initialState, actions.roomOpen(active))
    ).toEqual({
      ...initialState,
      active
    });
  });

  it('reducer handles first room add and sets that as active', () => {
    const newRooms = [rooms[0]]

    expect(
      reducer(initialState, actions.roomsSync(newRooms))
    ).toEqual({
      active: newRooms[0].id,
      rooms: newRooms
    });
  });

  it('if the active room was deleted reducer sets first room from the collection as active', () => {
    const state = {
      active: 'room-2',
      rooms
    };

    const newRooms = [
      rooms[0],
      rooms[2]
    ];

    expect(
      reducer(state, actions.roomsSync(newRooms))
    ).toEqual({
      active: newRooms[0].id,
      rooms: newRooms
    });
  });

  it('all rooms was deleted', () => {
    const state = {
      active: 'room-2',
      rooms
    };

    expect(
      reducer(state, actions.roomsSync([]))
    ).toEqual({
      active: '',
      rooms: []
    });
  });
})
