import { types } from '../actions/login';

export const initialState = {
  loading: true,
  loggedIn: false,
  user: {
    uid: '',
    displayName: '',
  }
}

function loginReducer (state = initialState, action) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
    case types.LOGOUT.REQUEST:
      return {
        ...state,
        loading: true
      }
    case types.LOGIN.SUCCESS:
      return {
        ...state,
        loading: false,
        loggedIn: true,
        user: action.user
      }
    case types.LOGIN.FAILURE:
    case types.LOGOUT.SUCCESS:
      return {
        ...initialState,
        loading: false,
      }
    case types.LOGOUT.FAILURE:
      return {
        ...state,
        loading: false
      }
    case types.USER.SYNC:
      return {
        ...state,
        loading: false,
        loggedIn: action.user != null,
        user: action.user
      }
    default:
      return state
  }
}

export default loginReducer;
